package p

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"os"
	"strings"
)

const responseType string = "ephemeral"
const errorURL string = "https://httpbin.org/status/500"
const succesURL string = "https://httpbin.org/status/200"
const oauthURL string = "https://slack.com/api/oauth.v2.access"

// OAuth handles bot registration
func OAuth(w http.ResponseWriter, r *http.Request) {
	// We must check if the error field is set, in case user cancels his requests
	error := r.URL.Query().Get("error")
	if error != "" {
		redirectURL := errorURL
		http.Redirect(w, r, redirectURL, http.StatusSeeOther)
	}

	// We require a code
	code := r.URL.Query().Get("code")
	if code == "" {
		redirectURL := errorURL
		http.Redirect(w, r, redirectURL, http.StatusSeeOther)
	}

	// Preparing to send a POST to slack's Oauth access page.
	hc := http.Client{}
	routerURL := oauthURL

	// Create the form
	form := url.Values{}
	form.Add("code", code)
	form.Add("client_id", os.Getenv("SLACK_OAUTH_CLIENT_ID"))
	form.Add("client_secret", os.Getenv("SLACK_OAUTH_CLIENT_SECRET"))

	req, err := http.NewRequest("POST", routerURL, strings.NewReader(form.Encode()))
	if err != nil {
		fmt.Println(err)
		redirectURL := errorURL
		http.Redirect(w, r, redirectURL, http.StatusSeeOther)
		return
	}

	req.PostForm = form
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	// Do the request
	res, err := hc.Do(req)
	if err != nil {
		fmt.Println(err)
		redirectURL := errorURL
		http.Redirect(w, r, redirectURL, http.StatusSeeOther)
		return
	}

	type jsonOauthResult struct {
		Ok bool `json:"ok"`
	}
	// Get the response
	decoder := json.NewDecoder(res.Body)
	var oauthResult jsonOauthResult
	err = decoder.Decode(&oauthResult)
	if err != nil {
		fmt.Println(err)
		redirectURL := errorURL
		http.Redirect(w, r, redirectURL, http.StatusSeeOther)
		return
	}

	// If the response was not OK, redirect for an error
	if oauthResult.Ok != true {
		fmt.Println("Oauth response was not TRUE")
		redirectURL := errorURL
		http.Redirect(w, r, redirectURL, http.StatusSeeOther)
		return
	}

	http.Redirect(w, r, succesURL, http.StatusSeeOther)
}
