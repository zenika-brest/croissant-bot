# Slack Bot to order Croissant

## Command

The bot is called with the `/croissant` slash command.

## Deployment

```bash
# Authenticate on GCP and store authentication info in the gcloud-config container
docker run -ti --name gcloud-config gcr.io/google.com/cloudsdktool/cloud-sdk gcloud auth login

# Deploy the cloud-function
SLACK_SIGNING_SECRET=xxx SLACK_OAUTH_TOKEN=xxx ./deploy.sh
```
