#!/usr/bin/env bash
set -euo pipefail

SLACK_SIGNING_SECRET=${SLACK_SIGNING_SECRET:?}
SLACK_OAUTH_TOKEN=${SLACK_OAUTH_TOKEN:?}

docker run --rm --volumes-from gcloud-config -v "$PWD":/app/ -w /app \
  gcr.io/google.com/cloudsdktool/cloud-sdk \
  gcloud --project hacker-vaillant functions deploy \
  --region europe-west1 croissant-bot --trigger-http  --runtime go113 --entry-point Croissant --allow-unauthenticated \
  --set-env-vars=SLACK_SIGNING_SECRET="$SLACK_SIGNING_SECRET" --set-env-vars=SLACK_OAUTH_TOKEN="$SLACK_OAUTH_TOKEN"
