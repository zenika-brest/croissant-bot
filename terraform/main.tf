provider "google" {
  project = var.project_id
  region  = var.region
  zone    = var.zone
}

resource "google_storage_bucket" "default" {
  name          = var.project_id
  force_destroy = false
  location      = "EU"
  storage_class = "STANDARD"
  versioning {
    enabled = true
  }
}

# Zip up our code so that we can store it for deployment.
data "archive_file" "source" {
  type        = "zip"
  source_dir  = "../function/"
  output_path = "../function.zip"
}

# This bucket will host the zipped file.
resource "google_storage_bucket" "bucket" {
  name     = "${var.project_id}-croissant-bot"
  location = var.region
}

# Add the zipped file to the bucket.
resource "google_storage_bucket_object" "zip" {
  # Use an MD5 here. If there's no changes to the source code, this won't change either.
  # We can avoid unnecessary redeployments by validating the code is unchanged, and forcing
  # a redeployment when it has!
  name   = "${data.archive_file.source.output_md5}.zip"
  bucket = google_storage_bucket.bucket.name
  source = data.archive_file.source.output_path
}

resource "google_cloudfunctions_function" "croissant-bot" {
  available_memory_mb   = 128
  source_archive_bucket = google_storage_bucket.bucket.name
  source_archive_object = "${data.archive_file.source.output_md5}.zip"

  entry_point = "Croissant"
  environment_variables = {
    "SLACK_OAUTH_TOKEN"    = var.slack_oauth_token
    "SLACK_SIGNING_SECRET" = var.slack_signing_secret
  }

  https_trigger_security_level = "SECURE_OPTIONAL"
  https_trigger_url            = "https://europe-west1-hacker-vaillant.cloudfunctions.net/croissant-bot"
  ingress_settings             = "ALLOW_ALL"
  labels = {
    "deployment-tool" = "cli-gcloud"
  }
  max_instances         = 0
  min_instances         = 0
  name                  = "croissant-bot"
  project               = "hacker-vaillant"
  region                = "europe-west1"
  runtime               = "go116"
  service_account_email = google_service_account.function-sa.email
  timeout               = 60
  trigger_http          = true

  timeouts {}
}

# IAM Configuration. This allows unauthenticated, public access to the function.
# Change this if you require more control here.
resource "google_cloudfunctions_function_iam_member" "invoker" {
  project        = google_cloudfunctions_function.croissant-bot.project
  region         = google_cloudfunctions_function.croissant-bot.region
  cloud_function = google_cloudfunctions_function.croissant-bot.name

  role   = "roles/cloudfunctions.invoker"
  member = "allUsers"
}

# This is the service account in which the function will act as.
resource "google_service_account" "function-sa" {
  account_id   = "function-sa"
  description  = "Controls the workflow for the cloud pipeline"
  display_name = "function-sa"
  project      = var.project_id
}
