variable "project_id" {
  default = "hacker-vaillant"
}

variable "region" {
  default = "europe-west1"
}

variable "zone" {
  default = "europe-west1-d"
}


variable "slack_oauth_token" {

}

variable "slack_signing_secret" {

}
