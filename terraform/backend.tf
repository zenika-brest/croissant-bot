terraform {
  backend "gcs" {
    bucket = "hacker-vaillant"
    prefix = "terraform/state"
  }
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "4.27.0"
    }
  }
}
